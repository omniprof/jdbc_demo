package com.kenfogel.jdbc_demo.persistence;

import com.kenfogel.jdbc_demo.beans.FishData;
import java.sql.SQLException;
import java.util.List;

/**
 * Interface for CRUD methods
 *
 * @author Ken
 */
public interface FishDAO {

    // Create
    public int create(FishData fishData) throws SQLException;

    // Read
    public List<FishData> findAll() throws SQLException;

    public FishData findID(int id) throws SQLException;

    public List<FishData> findDiet(String diet) throws SQLException;

    // Update
    public int update(FishData fishData) throws SQLException;

    // Delete
    public int delete(int ID) throws SQLException;
}
